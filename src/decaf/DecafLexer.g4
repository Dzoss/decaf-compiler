lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

PARESQ: '(';
PARDIR: ')';

CLASS: 'class';

PROGRAM: 'Program';

LCURLY : '{';
RCURLY : '}';

COLE: '[';
COLD: ']';

VIRG : ',' ; 
POINTV: ';';

BOOLEANVALOR: (KEYWORDTRUE | KEYWORDFALSE);
KEYWORDBOOL:'boolean';
KEYWORDBREAK:'break';
KEYWORDCALLOUT:'callout';
KEYWORDCONTINUE:'continue';
KEYWORDELSE:'else';
KEYWORDFALSE:'false';
KEYWORDFOR:'for';
KEYWORDINT:'int';
KEYWORDRETURN:'return';
KEYWORDTRUE:'true';
KEYWORDVOID:'void';
KEYWORDIF:'if';

HEXINIT: '0x';
HEX : HEXINIT [0-9a-fA-F];
NUMBER: [0-9]+;




MENOS: '-';
ARITHOPERATOR: ('+' |'-'| '*' | '/' | '%');
RELOPERATOR: '<' | '>' | '<=' | '>=';
EQOPERATOR: ('==' | '!=');
CONDOPERATOR: '&&' | '||';
MAISIG: '+=';
IGUAL: '=';
MENOSIG: '-=';
EXCLAM: '!';

CHAR : '\'' (ESC|C) '\'';

ID  : [a-zA-Z_][a-zA-Z0-9_]*; 

STRING : '"' (ESC|C)* '"';

WS_ : (' ' | '\n' | '\t')+ -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

fragment
ESC :  '\\' ('n'|'"'|'t'|'\\'|'\'');
fragment
C: [a-zA-Z0-9 !#-&(-/:-@^-`{-~];
















