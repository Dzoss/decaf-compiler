parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: CLASS PROGRAM LCURLY field_decl* method_decl* RCURLY EOF;

field_decl: ( decl | decl COLE (int_literal)+ COLD(VIRG (decl COLE int_literal COLD | decl))*) POINTV;  

//decl field* POINTV | decl COLE int_literal COLD field* POINTV;

//field: (VIRG (decl | decl COLE int_literal COLD));

method_decl: (type | KEYWORDVOID) ID PARESQ (decl (VIRG decl)*)* PARDIR block;

block: LCURLY var_decl* statement* RCURLY; 

var_decl: decl (VIRG ID)* POINTV;

decl: type ID;

type: KEYWORDBOOL | KEYWORDINT;	

statement: (location assign_op expr POINTV 
| method_call POINTV 
| KEYWORDIF expr block (KEYWORDELSE block)* 
| KEYWORDFOR ID IGUAL expr VIRG expr block 
| return_met POINTV 
| KEYWORDBREAK POINTV 
| KEYWORDCONTINUE POINTV 
| block);
  
assign_op: IGUAL | MAISIG | MENOSIG;

method_call: method_name PARESQ (expr (VIRG expr)*)* PARDIR | KEYWORDCALLOUT PARESQ STRING (VIRG callout_arg (VIRG callout_arg)*)* PARDIR;

method_name: ID;

location: ID | ID  COLE expr COLD;

expr: location (expr)*
	| method_call 
	| literal 
	| expr bin_op expr 
	| MENOS expr 
	| EXCLAM expr
    	| PARESQ expr PARDIR; 

return_met: KEYWORDRETURN (expr)*;

callout_arg: (expr | STRING);

bin_op: (ARITHOPERATOR | RELOPERATOR | EQOPERATOR | CONDOPERATOR);

literal: (int_literal | char_literal | bool_literal);

int_literal: (NUMBER | HEX);

bool_literal: BOOLEANVALOR;

char_literal: CHAR;
